#!/bin/sh

DOCKERD_DIR="/usr/local/AppCentral/docker-ce/"
# disable iptables forward for vpn connection
DOCKERD_OPT="-H unix:///var/run/docker.sock -H tcp://0.0.0.0:2375 -g /usr/local/AppCentral/docker-ce/docker_lib/"
DOCKERD_PID="/var/run/docker.pid"
# DOCKER_RAMDISK muse be true
export DOCKER_RAMDISK=true

case $1 in
    start) 
		echo "Starting Docker daemon..." 
		# start rpcbind for aria2 rpc server
		/usr/builtin/sbin/rpcbind
		$DOCKERD_DIR/bin/cgroupfs-mount
		$DOCKERD_DIR/bin/dockerd $DOCKERD_OPT &
		sleep 10
		/usr/builtin/sbin/iptables -P FORWARD ACCEPT 		
	;;  
	stop)  
		echo "Stopping Docker daemon..."
		docker stop $(docker ps -a -q)
	 	docker_pid=`cat $DOCKERD_PID`
        kill $docker_pid
	;;  
	reload) 
		echo "Reload Docker daemon..." 
		docker stop $(docker ps -a -q)
		docker_pid=`cat $DOCKERD_PID`
		kill $docker_pid 
	    sleep 2 
		/usr/builtin/sbin/rpcbind		
	    $DOCKERD_DIR/bin/dockerd $DOCKERD_OPT &
	;;  
	*) 
		echo "usage: $0 {start|stop|reload}" 
	exit 1 
	;;  
esac 
exit 0
                                                                                                                                                                                                                                                                                                                
