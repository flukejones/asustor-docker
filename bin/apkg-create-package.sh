#/bin/sh

APP_NAME=$(basename $0)
# APP_PATH=$(dirname $(realpath $0))
APP_PATH=$(dirname ${PWD}/$0)
APKGTOOL=$APP_PATH/apkg-tools.py

# check if we got root permissions
if [ ! "$(whoami)" = "root" ]; then
	echo "[ERROR] script has to be run as root!"
	usage
	exit 1
fi

BASE_DIR=$APP_PATH/..
SOURCE_DIR="${BASE_DIR}/source"
DEST_DIR="${BASE_DIR}/build"
TMP_DIR=$(mktemp -d -t fluffyXXXXXX)

echo "[INFO] clean the output folder"
if [ -d ${DEST_DIR} ]; then
	rm -rf ${DEST_DIR}/*
else
	mkdir -p ${DEST_DIR}
fi
echo "[INFO] copy the files to SOURCE directory"
rsync -a --files-from="${BASE_DIR}/filelist" ${SOURCE_DIR}/ ${DEST_DIR}/
if [ $? -ne 0 ]; then
	echo "[ERROR] couldn't copy files... abort"
	exit 4
fi

# apkg-tools.py is quite a simple script - doesn't allow any output
# directory to be specified. So we have to do it yourselfs
echo "[INFO] finally create the APK package in ${TMP_DIR}"
CUR_DIR=$PWD
cd $TMP_DIR
chown -R root:root ${DEST_DIR}/*
$APKGTOOL create "${DEST_DIR}"
if [ $? -ne 0 ]; then
	cd $CUR_DIR
	echo "[ERROR] couldn't create APK... abort"
	exit 6
fi
echo "[INFO] copy and rename the created APK to ${CUR_DIR}"
OUTPUT_FILE=$(basename *x86-64.apk)
cp -a $TMP_DIR/*x86-64.apk $CUR_DIR/${OUTPUT_FILE%.apk}.apk
rm -f $TMP_DIR/*x86-64.apk
cd $CUR_DIR
